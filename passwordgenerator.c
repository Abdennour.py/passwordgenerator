#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>
#include <time.h>
#include <sys/types.h>
#include <sys/stat.h>

int main (int argc, char *argv[])
{
    srand(time(NULL));
    int passlength;
    char letters[] = "abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ123456789";

    printf("\033[0;32m");
    printf("Enter The Length or your password : ");
    printf("\033[0m");
    scanf("%d", &passlength);

    char password[passlength-1];
    for(int i = 0; i < passlength; i++)
    {
        password[i] = letters[rand() % strlen(letters)];
        printf("\033[0;36m");
        printf("%c", password[i]);
        printf("\033[0m");
    }

    // Save the password :
    int save;
    printf("\033[0;32m");
    printf("\nDo you want to save your password in file 🗃️ ?\n[1]-YES [2]-NO\nEnter The Number : ");
    printf("\033[0m");
    scanf("%d", &save);

    if (save == 1)
    {
        char path[100];
        printf("\033[0;32m");
        printf("Enter The File Path 📁 : ");
        printf("\033[0m");
        scanf("%s", &path);
        printf("\033[0;36m");
        printf("Your File Path is : %s\n", path);
        printf("\033[0m");
        FILE *pF = fopen(path, "w");
        fprintf(pF, "%s", password);
        fclose(pF);
        printf("\033[0;36m");
        printf("Save Done ✅");
        printf("\033[0m");
    }
    else if (save == 2)
    {
        printf("\033[0;36m");
        printf("Done ✅");
        printf("\033[0m");
    }
    return 0;

}
